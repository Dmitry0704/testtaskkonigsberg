package com.gaiduk.noteskotlin.ui

import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.gaiduk.noteskotlin.R
import com.ismaeldivita.chipnavigation.ChipNavigationBar
import kotlinx.android.synthetic.main.main.*

class MainActivity : AppCompatActivity() {

    private val fragmentHome: FragmentHome by lazy { FragmentHome() }
    private val fragmentClasses: FragmentClasses by lazy { FragmentClasses() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        bottomNavigationMenu.setItemEnabled(R.id.calendar, false)
        bottomNavigationMenu.setItemEnabled(R.id.star, false)
        bottomNavigationMenu.setItemSelected(R.id.home)

        bottomNavigationMenu.setOnItemSelectedListener(object :
            ChipNavigationBar.OnItemSelectedListener {
            override fun onItemSelected(id: Int) {
                switchFragment()
            }

        })

        mountFragment(fragmentHome)
    }

    private fun switchFragment() {
        val currentFragment =
            supportFragmentManager.findFragmentById(R.id.fragmentContainer)
        when (currentFragment) {
            is FragmentHome -> mountFragment(fragmentClasses)
            is FragmentClasses -> mountFragment(fragmentHome)
        }
    }

    private fun mountFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, fragment)
            .commitNow()
    }

}