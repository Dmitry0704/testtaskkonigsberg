package com.gaiduk.noteskotlin.ui

import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.gaiduk.noteskotlin.R
import com.gaiduk.noteskotlin.ui.base.BaseFragment

class FragmentClasses : BaseFragment() {

    companion object {
        fun getNewInstance(): FragmentClasses? {
            val fragment = FragmentClasses()
            val bundle = Bundle()
            fragment.arguments = bundle
            fragment.retainInstance = false
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewInflated(view: View, savedInstanceState: Bundle?) {
        var fragmentEditText = view.findViewById<TextView>(R.id.textViewEdit)
        fragmentEditText.text = "Fragment Classes"
    }

    override fun onInflateLayout(): Int {
        return R.layout.fragment_classes
    }
}