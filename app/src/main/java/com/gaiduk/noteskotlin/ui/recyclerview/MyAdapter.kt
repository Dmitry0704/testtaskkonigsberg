package com.gaiduk.noteskotlin.ui.recyclerview

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.gaiduk.noteskotlin.R

class MyAdapter(listArray: ArrayList<ItemModel>) :
    RecyclerView.Adapter<MyAdapter.ViewHolder>() {

    var listArrayR = listArray

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater =
            LayoutInflater.from(parent.context).inflate(
                R.layout.homework_item,
                parent,
                false
            )
        return ViewHolder(inflater)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var itemModel = listArrayR.get(position)
        holder.bind(itemModel)
    }

    override fun getItemCount(): Int {
        return listArrayR.size
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val someTextView = view.findViewById<TextView>(R.id.someTextView)

//        val imageViewFish = view.findViewById<ImageView>(R.id.imageViewFish)
//        val tvFishName = view.findViewById<TextView>(R.id.tvFishName)
//        val tvFishDescription = view.findViewById<TextView>(R.id.tvFishDescription)

        fun bind(itemModel: ItemModel) {
            someTextView.text = itemModel.someText
//            imageViewFish.setImageResource(itemModel.imageId)
//            tvFishName.text = itemModel.titleText
//            tvFishDescription.text = itemModel.contentText
        }
    }
}