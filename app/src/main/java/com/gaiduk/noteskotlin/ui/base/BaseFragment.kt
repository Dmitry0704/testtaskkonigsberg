package com.gaiduk.noteskotlin.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment

open abstract class BaseFragment : Fragment() {

    var view1: View? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        view1 = inflater.inflate(onInflateLayout(), container, false)
        onViewInflated(view1!!, savedInstanceState)
        return view1
    }

    abstract fun onViewInflated(view: View, savedInstanceState: Bundle?)

    @LayoutRes
    abstract fun onInflateLayout(): Int
}