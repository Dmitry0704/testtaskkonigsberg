package com.gaiduk.noteskotlin.ui

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gaiduk.noteskotlin.R
import com.gaiduk.noteskotlin.ui.base.BaseFragment
import com.gaiduk.noteskotlin.ui.recyclerview.ItemModel
import com.gaiduk.noteskotlin.ui.recyclerview.MyAdapter

class FragmentHome: BaseFragment() {

    private var myAdapter: MyAdapter? = null

    companion object {
        fun getNewInstance(): FragmentHome? {
            val fragment = FragmentHome()
            val bundle = Bundle()
            fragment.arguments = bundle
            fragment.retainInstance = false
            return fragment
        }
    }

    override fun onViewInflated(view: View, savedInstanceState: Bundle?) {
        var list = ArrayList<ItemModel>()
        list.add(ItemModel("Карась"))
        list.add(ItemModel("Щука"))
        list.add(ItemModel("Налим"))
        list.add(ItemModel("Сом"))
        val recyclerView = view.findViewById(R.id.recyclerView) as RecyclerView
        recyclerView.hasFixedSize()
        recyclerView.layoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        myAdapter = MyAdapter(listArray = list)
        recyclerView.adapter = myAdapter
    }

    override fun onInflateLayout(): Int {
        return R.layout.fragment_home
    }
}