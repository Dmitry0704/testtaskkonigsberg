//package com.gaiduk.noteskotlin.dagger
//
//import com.gaiduk.noteskotlin.ui.MainActivity
//import com.gaiduk.noteskotlin.dagger.module.ApplicationModule
//import com.gaiduk.noteskotlin.dagger.module.NavigationModule
//import com.gaiduk.noteskotlin.ui.FragmentEdit
//import com.gaiduk.noteskotlin.ui.FragmentNotes
//import dagger.Component
//import javax.inject.Singleton
//
//@Singleton
//@Component(modules = [ApplicationModule::class, NavigationModule::class])
//interface AppComponent {
//
//    fun inject(activity: MainActivity)
//
//    fun inject(fragment: FragmentNotes)
//
//    fun inject(fragment: FragmentEdit)
//
//}