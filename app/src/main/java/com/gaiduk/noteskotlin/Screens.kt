package com.gaiduk.noteskotlin

class Screens {
    companion object {
        val FRAGMENT_NOTES = "FRAGMENT_NOTES"
        val FRAGMENT_EDIT = "FRAGMENT_EDIT"
    }
}